<?php

namespace Drupal\paynl_payment\Helpers;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Paynl\Config;
use Paynl\Paymentmethods;
use Paynl\Transaction;

/**
 * Class PaynlSdkHelper.
 *
 * @package Drupal\paynl_payment\Helpers
 */
class PaynlSdkHelper {

  use StringTranslationTrait;

  /**
   * Pay.nl SDK Config class.
   *
   * @var \Paynl\Config
   */
  protected $config;

  /**
   * Pay.nl SDK Paymentmethods class.
   *
   * @var \Paynl\Paymentmethods
   */
  protected $methods;

  /**
   * Pay.nl SDK Transaction class.
   *
   * @var \Paynl\Transaction
   */
  protected $transaction;

  /**
   * Configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  const PAYNL_TRANSACTION_PHASE_RETURN = 'return';
  const PAYNL_TRANSACTION_PHASE_EXCHANGE = 'exchange';

  /**
   * PaynlSdkHelper constructor.
   *
   * @param \Paynl\Config $config
   *   Pay.nl SDK Config class.
   * @param \Paynl\Paymentmethods $methods
   *   Pay.nl SDK Paymentmethods class.
   * @param \Paynl\Transaction $transaction
   *   Pay.nl SDK Transaction class.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configuration factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   */
  public function __construct(
    Config $config,
    Paymentmethods $methods,
    Transaction $transaction,
    ConfigFactoryInterface $configFactory,
    MessengerInterface $messenger
  ) {
    $this->config = $config;
    $this->methods = $methods;
    $this->transaction = $transaction;
    $this->configFactory = $configFactory;
    $this->messenger = $messenger;

    $this->initialize();
  }

  /**
   * Checks whether the helper can connect to the Pay.nl API.
   *
   * @return bool
   *   True if a successful connection can be set up and false otherwise.
   */
  public function hasConnection() {
    if ($this->getMethods() !== NULL) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Checks whether test mode is enabled in the configuration.
   *
   * @return bool
   *   True if test mode is enabled and false otherwise.
   */
  public function hasTestMode() {
    $config = $this->configFactory->get('paynl_payment.paynl_config');

    return (bool) $config->get('test_mode');
  }

  /**
   * Gets a list of available payment methods for the configured service ID.
   *
   * @return null|array
   *   An array of available payment methods or null on failure.
   */
  public function getMethods() {
    $methods = NULL;

    try {
      $methodList = ($this->methods)::getList();
      $methods = ['' => $this->t('Show payment method selection page on the Pay.nl website')];
      foreach ($methodList as $method) {
        $methods[$method['id']] = $method['visibleName'];
      }
    }
    catch (\Exception $e) {
      $this->logException($e);
    }

    return $methods;
  }

  /**
   * Gets a list of available banks for the configured service ID.
   *
   * @return null|array
   *   An array of available banks or null on failure.
   */
  public function getBanks() {
    $banks = NULL;

    try {
      $bankList = ($this->methods)::getBanks();
      $banks = [];
      foreach ($bankList as $bank) {
        $banks[$bank['id']] = $bank['visibleName'];
      }
    }
    catch (\Exception $e) {
      $this->logException($e);
    }

    return $banks;
  }

  /**
   * Starts a Pay.nl transaction.
   *
   * @param array $options
   *   An array of options for the transaction.
   *
   * @return null|\Paynl\Result\Transaction\Start
   *   The transaction start on success or null on failure.
   */
  public function startTransaction(array $options) {
    $transaction = NULL;

    try {
      $transaction = ($this->transaction)::start($options);
    }
    catch (\Exception $e) {
      $this->logException($e);
    }

    return $transaction;
  }

  /**
   * Gets a transaction based on the transaction phase or a transaction ID.
   *
   * @param string $phase
   *   The phase for the transaction. Any undefined phase will load by ID.
   * @param null|string $transactionId
   *   The ID of a transaction. Used in case an undefined phase is used.
   *
   * @return null|\Paynl\Result\Transaction\Transaction
   *   A transaction if one is found or null otherwise.
   */
  public function getTransaction($phase, $transactionId = NULL) {
    $transaction = NULL;

    switch ($phase) {
      case self::PAYNL_TRANSACTION_PHASE_RETURN:
        try {
          $transaction = ($this->transaction)::getForReturn();
        }
        catch (\Exception $e) {
          $this->logException($e);
        }
        break;

      case self::PAYNL_TRANSACTION_PHASE_EXCHANGE:
        try {
          $transaction = ($this->transaction)::getForExchange();
        }
        catch (\Exception $e) {
          $this->logException($e);
        }
        break;

      default:
        try {
          $transaction = ($this->transaction)::get($transactionId);
        }
        catch (\Exception $e) {
          $this->logException($e);
        }
    }

    return $transaction;
  }

  /**
   * Returns whether the transaction corresponding to the request is paid.
   *
   * @param string $phase
   *   The phase for the transaction.
   *
   * @return bool
   *   True if the transaction corresponding to the current request is paid and
   *   false otherwise.
   */
  public function isPaid($phase) {
    return $this->getTransaction($phase)
      ->isPaid();
  }

  /**
   * Returns whether the transaction corresponding to the request is canceled.
   *
   * @param string $phase
   *   The phase for the transaction.
   *
   * @return bool
   *   True if the transaction corresponding to the current request is canceled
   *   and false otherwise.
   */
  public function isCanceled($phase) {
    return $this->getTransaction($phase)
      ->isCanceled();
  }

  /**
   * Logs an exception and adds a message for the end user.
   *
   * @param \Exception $exception
   *   The exception that is thrown.
   */
  public function logException(\Exception $exception) {
    watchdog_exception('paynl_payment', $exception);
    $this->messenger->addMessage(
      $this->t('An error occurred while handling your request. Try again and contact a system administrator if the problem persists.'),
      'error'
    );
  }

  /**
   * Initializes the client for interaction with the Pay.nl API.
   */
  protected function initialize() {
    // Set configuration for client.
    $config = $this->configFactory->get('paynl_payment.paynl_config');

    try {
      ($this->config)::setTokenCode($config->get('token_code'));
      ($this->config)::setApiToken($config->get('api_key'));
      ($this->config)::setServiceId($config->get('service_id'));
    }
    catch (\Exception $e) {
      $this->logException($e);
    }
  }

}
