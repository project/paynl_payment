<?php

namespace Drupal\paynl_payment;

use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\payment\Entity\PaymentInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class PaynlPaymentMethodCountryAccessEvent.
 *
 * @package Drupal\paynl_payment
 */
class PaynlPaymentMethodCountryAccessEvent extends Event {

  /**
   * Payment entity.
   *
   * @var \Drupal\payment\Entity\PaymentInterface
   */
  protected $payment;

  /**
   * Payment method plugin definition.
   *
   * @var array
   */
  protected $pluginDefinition;

  /**
   * Access result object.
   *
   * @var \Drupal\Core\Access\AccessResultInterface
   */
  protected $access;

  /**
   * PaynlPaymentMethodCountryAccessEvent constructor.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   Payment entity.
   * @param array $pluginDefinition
   *   Payment method plugin definition.
   */
  public function __construct(PaymentInterface $payment, array $pluginDefinition) {
    $this->payment = $payment;
    $this->pluginDefinition = $pluginDefinition;

    $this->access = new AccessResultAllowed();
  }

  /**
   * Gets the name of this event.
   *
   * @return string
   *   The name of this event.
   */
  public static function getEventName() {
    return 'paynl_payment.country_access';
  }

  /**
   * Gets the payment for which this event was fired.
   *
   * @return \Drupal\payment\Entity\PaymentInterface
   *   Payment entity.
   */
  public function getPayment() {
    return $this->payment;
  }

  /**
   * Gets the definition of the payment plugin for which this event was fired.
   *
   * @return array
   *   Payment method plugin definition.
   */
  public function getPluginDefinition() {
    return $this->pluginDefinition;
  }

  /**
   * Sets the access result.
   *
   * Subscribers can use this method to forbid access only.
   *
   * @param \Drupal\Core\Access\AccessResultInterface $access
   *   Access result object.
   */
  public function setAccess(AccessResultInterface $access) {
    if ($access->isForbidden()) {
      $this->access = $access;
    }
  }

  /**
   * Gets the access result.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Access result object.
   */
  public function getAccess() {
    return $this->access;
  }

}
