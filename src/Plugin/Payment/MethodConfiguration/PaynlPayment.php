<?php

namespace Drupal\paynl_payment\Plugin\Payment\MethodConfiguration;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Locale\CountryManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\payment\Plugin\Payment\MethodConfiguration\PaymentMethodConfigurationBase;
use Drupal\paynl_payment\Helpers\PaynlSdkHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PaynlPayment.
 *
 * @PaymentMethodConfiguration(
 *   description = @Translation("A payment method provided by Pay.nl."),
 *   id = "paynl_payment",
 *   label = @Translation("Pay.nl")
 * )
 *
 * @package Drupal\paynl_payment\Plugin\Payment\MethodConfiguration
 */
class PaynlPayment extends PaymentMethodConfigurationBase implements ContainerFactoryPluginInterface {

  /**
   * Pay.nl SDK helper.
   *
   * @var \Drupal\paynl_payment\Helpers\PaynlSdkHelper
   */
  protected $paynlSdkHelper;

  /**
   * Country manager service.
   *
   * @var \Drupal\Core\Locale\CountryManager
   */
  protected $countryManager;

  /**
   * PaynlPayment constructor.
   *
   * @param mixed[] $configuration
   *   Plugin configuration.
   * @param string $pluginId
   *   Plugin ID.
   * @param mixed[] $pluginDefinition
   *   Plugin definition.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translator.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Locale\CountryManager $countryManager
   *   Country manager service.
   * @param \Drupal\paynl_payment\Helpers\PaynlSdkHelper $paynlSdkHelper
   *   Pay.nl SDK helper.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    TranslationInterface $stringTranslation,
    ModuleHandlerInterface $moduleHandler,
    CountryManager $countryManager,
    PaynlSdkHelper $paynlSdkHelper
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $stringTranslation, $moduleHandler);

    $this->paynlSdkHelper = $paynlSdkHelper;
    $this->countryManager = $countryManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('string_translation'),
      $container->get('module_handler'),
      $container->get('country_manager'),
      $container->get('paynl_payment.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $defaultConfiguration = [
      'method' => '',
      'fee_type' => 'amount',
      'fee_value' => 0,
      'min_amount' => 0,
      'max_amount' => 0,
      'allowed_countries' => [],
      'weight' => 0,
    ];

    return parent::defaultConfiguration() + $defaultConfiguration;
  }

  /**
   * Sets the method to be used for processing the payment.
   *
   * @param string $method
   *   The method to set.
   *
   * @return $this
   */
  public function setMethod($method) {
    $this->configuration['method'] = $method;

    return $this;
  }

  /**
   * Gets the method to be used for processing the payment.
   *
   * @return string
   *   The method ID of the method to use.
   */
  public function getMethod() {
    return $this->configuration['method'];
  }

  /**
   * Sets the type of fee to be paid for processing the payment.
   *
   * @param string $type
   *   The fee type to set.
   *
   * @return $this
   */
  public function setFeeType($type) {
    $this->configuration['fee_type'] = $type;

    return $this;
  }

  /**
   * Gets the type of fee to be paid for processing the payment.
   *
   * @return string
   *   The fee type.
   */
  public function getFeeType() {
    return $this->configuration['fee_type'];
  }

  /**
   * Sets the fee to be paid for processing the payment.
   *
   * @param float $value
   *   The fee value.
   *
   * @return $this
   */
  public function setFee($value) {
    $this->configuration['fee_value'] = $value;

    return $this;
  }

  /**
   * Gets the fee to be paid for processing the payment.
   *
   * @return float
   *   The fee value.
   */
  public function getFee() {
    return $this->configuration['fee_value'];
  }

  /**
   * Sets the minimum order amount required to use this payment method.
   *
   * @param float $value
   *   The order amount.
   *
   * @return $this
   */
  public function setMinOrderAmount($value) {
    $this->configuration['min_amount'] = $value;

    return $this;
  }

  /**
   * Gets the minimum order amount required to use this payment method.
   *
   * @return float
   *   The order amount.
   */
  public function getMinOrderAmount() {
    return $this->configuration['min_amount'];
  }

  /**
   * Sets the maximum order amount required to use this payment method.
   *
   * @param float $value
   *   The order amount.
   *
   * @return $this
   */
  public function setMaxOrderAmount($value) {
    $this->configuration['max_amount'] = $value;

    return $this;
  }

  /**
   * Gets the maximum order amount required to use this payment method.
   *
   * @return float
   *   The order amount.
   */
  public function getMaxOrderAmount() {
    return $this->configuration['max_amount'];
  }

  /**
   * Sets the countries for which this payment method is allowed.
   *
   * @param float $value
   *   The fee value.
   *
   * @return $this
   */
  public function setAllowedCountries($value) {
    $this->configuration['allowed_countries'] = $value;

    return $this;
  }

  /**
   * Gets the countries for which this payment method is allowed.
   *
   * @return float
   *   The fee value.
   */
  public function getAllowedCountries() {
    return $this->configuration['allowed_countries'];
  }

  /**
   * Sets a weight that can be used for ordering payment methods.
   *
   * @param int $value
   *   The weight value.
   *
   * @return $this
   */
  public function setWeight($value) {
    $this->configuration['weight'] = $value;

    return $this;
  }

  /**
   * Gets the weight that can be used for ordering payment methods.
   *
   * @return in
   *   The weight value.
   */
  public function getWeight() {
    return $this->configuration['weight'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $formState) {
    $form = parent::buildConfigurationForm($form, $formState);
    $form['plugin_form'] = [
      '#process' => [[$this, 'processBuildConfigurationForm']],
      '#type' => 'container',
    ];

    return $form;
  }

  /**
   * Implements a form API #process callback.
   */
  public function processBuildConfigurationForm(array &$element, FormStateInterface $formState, array &$form) {
    $element['method'] = [
      '#default_value' => $this->getMethod(),
      '#description' => $this->t('The payment method used within the Pay.nl platform.'),
      '#title' => $this->t('Payment method'),
      '#type' => 'select',
      '#options' => $this->paynlSdkHelper->getMethods(),
    ];

    $element['fee_type'] = [
      '#default_value' => $this->getFeeType(),
      '#description' => $this->t('The type of payment fee to be paid.'),
      '#title' => $this->t('Payment fee type'),
      '#type' => 'select',
      '#options' => [
        'amount' => $this->t('Amount'),
        'percentage' => $this->t('Percentage'),
      ],
    ];
    $element['fee_value'] = [
      '#default_value' => $this->getFee(),
      '#description' => $this->t('The payment fee to be paid.'),
      '#title' => $this->t('Payment fee'),
      '#type' => 'number',
      '#attributes' => [
        'step' => '0.05',
        'min' => 0,
      ],
    ];

    $element['min_amount'] = [
      '#default_value' => $this->getMinOrderAmount(),
      '#description' => $this->t('The minimum order amount required to use this payment method. Use 0 for no minimum.'),
      '#title' => $this->t('Minimum order amount'),
      '#type' => 'number',
      '#attributes' => [
        'step' => '0.05',
        'min' => 0,
      ],
    ];
    $element['max_amount'] = [
      '#default_value' => $this->getMaxOrderAmount(),
      '#description' => $this->t('The maximum order amount required to use this payment method.. Use 0 for no maximum.'),
      '#title' => $this->t('Maximum order amount'),
      '#type' => 'number',
      '#attributes' => [
        'step' => '0.05',
        'min' => 0,
      ],
    ];

    $element['allowed_countries'] = [
      '#default_value' => $this->getAllowedCountries(),
      '#description' => $this->t('The countries for which this payment method is allowed. If no countries are selected the payment method is allowed for all countries.'),
      '#title' => $this->t('Allowed countries'),
      '#type' => 'select',
      '#options' => $this->countryManager->getList(),
      '#multiple' => TRUE,
    ];

    $element['weight'] = [
      '#default_value' => $this->getWeight(),
      '#description' => $this->t('The weight can be used by payment types for sorting the payment methods.'),
      '#title' => $this->t('Weight'),
      '#type' => 'number',
      '#attributes' => [
        'step' => '1',
        'min' => 0,
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $formState) {
    parent::submitConfigurationForm($form, $formState);

    $parents = $form['plugin_form']['method']['#parents'];
    array_pop($parents);
    $values = NestedArray::getValue($formState->getValues(), $parents);
    $this->setMethod($values['method']);
    $this->setFeeType($values['fee_type']);
    $this->setFee($values['fee_value']);
    $this->setMinOrderAmount($values['min_amount']);
    $this->setMaxOrderAmount($values['max_amount']);
    $this->setAllowedCountries($values['allowed_countries']);
    $this->setWeight($values['weight']);
  }

}
