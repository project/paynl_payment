<?php

namespace Drupal\paynl_payment\Plugin\Payment\Method;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;
use Drupal\payment\EventDispatcherInterface;
use Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface;
use Drupal\paynl_payment\Helpers\PaynlSdkHelper;
use Drupal\paynl_payment\PaynlPaymentMethodCountryAccessEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\payment\OperationResult;
use Drupal\payment\Plugin\Payment\Method\PaymentMethodBase;
use Drupal\payment\Response\Response;
use Symfony\Component\EventDispatcher\EventDispatcherInterface as CoreEventDispatcherInterface;

/**
 * Class PaynlPayment.
 *
 * @PaymentMethod(
 *   deriver = "Drupal\paynl_payment\Plugin\Payment\Method\PaynlPaymentDeriver",
 *   id = "paynl_payment",
 *   operations_provider = "\Drupal\paynl_payment\Plugin\Payment\Method\PaynlPaymentOperationsProvider",
 * )
 */
class PaynlPayment extends PaymentMethodBase {

  /**
   * Pay.nl SDK helper.
   *
   * @var \Drupal\paynl_payment\Helpers\PaynlSdkHelper
   */
  protected $paynlSdkHelper;

  /**
   * Messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The redirect URL for the off site payment.
   *
   * @var null|\Drupal\Core\Url
   */
  protected $redirectUrl = NULL;

  /**
   * Core event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $coreEventDispatcher;

  /**
   * Constructs a new instance.
   *
   * @param mixed[] $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed[] $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\payment\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Utility\Token $token
   *   The token API.
   * @param \Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface $payment_status_manager
   *   The payment status manager.
   * @param \Drupal\paynl_payment\Helpers\PaynlSdkHelper $paynlSdkHelper
   *   Pay.nl SDK helper.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $coreEventDispatcher
   *   Core event dispatcher.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    ModuleHandlerInterface $module_handler,
    EventDispatcherInterface $event_dispatcher,
    Token $token,
    PaymentStatusManagerInterface $payment_status_manager,
    PaynlSdkHelper $paynlSdkHelper,
    MessengerInterface $messenger,
    CoreEventDispatcherInterface $coreEventDispatcher
  ) {
    $configuration += $this->defaultConfiguration();
    $this->paynlSdkHelper = $paynlSdkHelper;
    $this->messenger = $messenger;
    $this->coreEventDispatcher = $coreEventDispatcher;
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $module_handler,
      $event_dispatcher,
      $token,
      $payment_status_manager
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('payment.event_dispatcher'),
      $container->get('token'),
      $container->get('plugin.manager.payment.status'),
      $container->get('paynl_payment.helper'),
      $container->get('messenger'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedCurrencies() {
    // Returning true allows all currencies.
    return TRUE;
  }

  /**
   * Gets the payment method.
   *
   * @return string
   *   The ID of the payment method.
   */
  public function getMethod() {
    return $this->pluginDefinition['method'];
  }

  /**
   * Sets the bank used for execution of the payment.
   *
   * @param string $bank
   *   The ID of the bank.
   *
   * @return $this
   */
  public function setBank($bank) {
    $this->configuration['bank'] = $bank;

    return $this;
  }

  /**
   * Gets the bank used for execution of the payment.
   *
   * @return string
   *   The ID of the bank.
   */
  public function getBank() {
    return $this->configuration['bank'];
  }

  /**
   * Gets the fee to be paid for the payment.
   *
   * @return array
   *   An associative array with the fee type and the fee value.
   */
  public function getFee() {
    return [
        'type' => $this->pluginDefinition['fee_type'],
        'value' => $this->pluginDefinition['fee_value'],
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $formState) {
    $elements = parent::buildConfigurationForm($form, $formState);

    if ($this->getMethod() == 10) {
      // For iDEAL let the customer select a bank.
      $elements['bank'] = [
        '#type' => 'select',
        '#title' => $this->t('Select your bank'),
        '#options' => $this->paynlSdkHelper->getBanks(),
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $formState) {
    if (isset($form['bank'])) {
      $parents = $form['bank']['#parents'];
      if (is_array($parents)) {
        array_pop($parents);
        $values = NestedArray::getValue($formState->getValues(), $parents);
        $this->setBank($values['bank']);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function doExecutePaymentAccess(AccountInterface $account) {
    return AccessResult::allowedIf(TRUE)
      ->andIf($this->methodAllowedForPaymentAmount())
      ->andIf($this->methodAllowedForPaymentCountry());
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentExecutionResult() {
    if ($this->redirectUrl) {
      $redirectUrl = $this->redirectUrl;
    }
    else {
      // Something went wrong. Redirect to the current page.
      $redirectUrl = Url::fromRoute('<current>');
    }

    return new OperationResult(new Response($redirectUrl));
  }

  /**
   * {@inheritdoc}
   */
  protected function doExecutePayment() {
    try {
      $payment = $this->getPayment();

      // Get line items to pass them to Pay.nl.
      // Todo: determine the tax amount.
      $line_items = [];
      foreach ($payment->getLineItems() as $line_item) {
        $line_items[] = [
          'id' => $line_item->getName(),
          'name' => $line_item->getDescription(),
          'price' => $line_item->getAmount(),
          'tax' => $line_item->getAmount() - $line_item->getAmount(),
          'qty' => $line_item->getQuantity(),
        ];
      }

      // Add payment fee if applicable.
      $fee = 0;
      if ($this->getFee()['value'] != 0) {
        if ($this->getFee()['type'] == 'percentage') {
          $fee = (float) $this->getFee()['value'] / 100 * $payment->getAmount();
        }
        else {
          $fee = (float) $this->getFee()['value'];
        }

        $line_items[] = [
          'id' => 'payment_fee',
          'name' => $this->t('Payment fee'),
          'price' => $fee,
          'tax' => 0,
          'qty' => 1,
        ];
      }

      try {
        // Start transaction.
        $options = [
          'amount' => $payment->getAmount() + $fee,
          'returnUrl' => Url::fromRoute(
            'paynl_payment.return',
            ['payment' => $payment->id()],
            ['absolute' => TRUE]
          )->toString(),
          'exchangeUrl' => Url::fromRoute(
            'paynl_payment.exchange',
            ['payment' => $payment->id()],
            ['absolute' => TRUE]
          )->toString(),
          'description' => $payment->id(),
          'products' => $line_items,
          'language' => strtoupper(\Drupal::languageManager()->getCurrentLanguage()->getId()),
          'ipaddress' => \Drupal::request()->getClientIp(),
          'testmode' => $this->paynlSdkHelper->hasTestMode(),
        ];
        if ($this->getMethod()) {
          $options['paymentMethod'] = (int) $this->getMethod();

          // For iDEAL we also need to pass in the bank.
          if ($this->getMethod() == 10) {
            $options['bank'] = (int) $this->getBank();
          }
        }
        $transaction = $this->paynlSdkHelper->startTransaction($options);

        // Set redirect URL for off site payment.
        $this->redirectUrl = Url::fromUri($transaction->getRedirectUrl());
      }
      catch (\Exception $e) {
        $this->paynlSdkHelper->logException($e);
      }
    }
    catch (\Exception $e) {
      $this->paynlSdkHelper->logException($e);
    }
  }

  /**
   * Determines if the method is allowed for the total amount of the payment.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   An access result object.
   */
  protected function methodAllowedForPaymentAmount() {
    $payment = $this->getPayment();
    $pluginDefinition = $this->getPluginDefinition();

    if ($this->amountBelowMinimum($payment->getAmount(), $pluginDefinition['min_amount'])
      || $this->amountAboveMaximium($payment->getAmount(), $pluginDefinition['max_amount'])) {
      return AccessResult::forbidden();
    }

    return AccessResult::allowed();
  }

  /**
   * Determines if the method is allowed for the country of the payment.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   An access result object.
   */
  protected function methodAllowedForPaymentCountry() {
    $payment = $this->getPayment();
    $pluginDefinition = $this->getPluginDefinition();

    // Allow modules to check access based on countries for payment types that
    // have this information.
    $event = new PaynlPaymentMethodCountryAccessEvent($payment, $pluginDefinition);
    $this->coreEventDispatcher->dispatch(
      PaynlPaymentMethodCountryAccessEvent::getEventName(),
      $event
    );

    return $event->getAccess();
  }

  /**
   * Determines if a certain value is below a minimum.
   *
   * @param float $amount
   *   The amount to test.
   * @param float $minimum
   *   The minimum to test for.
   *
   * @return bool
   *   True if the amount is below the minimum. False otherwise.
   */
  protected function amountBelowMinimum($amount, $minimum) {
    return $amount < $minimum;
  }

  /**
   * Determines if a certain value is above a maximum.
   *
   * @param float $amount
   *   The amount to test.
   * @param float $maximum
   *   The minimum to test for.
   *
   * @return bool
   *   True if the amount is above the maximum and the maximum is greater
   *   than 0. False otherwise.
   */
  protected function amountAboveMaximium($amount, $maximum) {
    // A maximum of 0 is considered as no maximum.
    if ($maximum == 0) {
      return FALSE;
    }

    return $amount > $maximum;
  }

}
