<?php

namespace Drupal\paynl_payment\Plugin\Payment\Method;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\payment\Plugin\Payment\MethodConfiguration\PaymentMethodConfigurationManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PaynlPaymentDeriver.
 *
 * We need this class to be able to use the option to enable / disable this
 * payment method through a payment method configuration form.
 *
 * @package Drupal\paynl_payment\Plugin\Payment\Method
 */
class PaynlPaymentDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The payment method configuration manager.
   *
   * @var \Drupal\payment\Plugin\Payment\MethodConfiguration\PaymentMethodConfigurationManagerInterface
   */
  protected $paymentMethodConfigurationManager;

  /**
   * The payment method configuration storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $paymentMethodConfigurationStorage;

  /**
   * PaynlPaymentDeriver constructor.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $paymentMethodConfigurationStorage
   *   The payment method configuration storage.
   * @param \Drupal\payment\Plugin\Payment\MethodConfiguration\PaymentMethodConfigurationManagerInterface $paymentMethodConfigurationManager
   *   The payment method configuration manager.
   */
  public function __construct(
    EntityStorageInterface $paymentMethodConfigurationStorage,
    PaymentMethodConfigurationManagerInterface $paymentMethodConfigurationManager
  ) {
    $this->paymentMethodConfigurationStorage = $paymentMethodConfigurationStorage;
    $this->paymentMethodConfigurationManager = $paymentMethodConfigurationManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $basePluginId) {
    $entityTypeManager = $container->get('entity_type.manager');

    return new static(
      $entityTypeManager->getStorage('payment_method_configuration'),
      $container->get('plugin.manager.payment.method_configuration')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($basePluginDefinition) {
    /** @var \Drupal\payment\Entity\PaymentMethodConfigurationInterface[] $paymentMethods */
    $paymentMethods = $this->paymentMethodConfigurationStorage->loadMultiple();
    foreach ($paymentMethods as $paymentMethod) {
      if ($paymentMethod->getPluginId() == 'paynl_payment') {
        /** @var \Drupal\paynl_payment\Plugin\Payment\MethodConfiguration\PaynlPayment $configurationPlugin */
        $configurationPlugin = $this->paymentMethodConfigurationManager->createInstance($paymentMethod->getPluginId(), $paymentMethod->getPluginConfiguration());
        $this->derivatives[$paymentMethod->id()] = [
          'id' => $basePluginDefinition['id'] . ':' . $paymentMethod->id(),
          'active' => $paymentMethod->status(),
          'label' => $paymentMethod->label(),
          'method' => $configurationPlugin->getMethod(),
          'fee_type' => $configurationPlugin->getFeeType(),
          'fee_value' => $configurationPlugin->getFee(),
          'min_amount' => $configurationPlugin->getMinOrderAmount(),
          'max_amount' => $configurationPlugin->getMaxOrderAmount(),
          'allowed_countries' => $configurationPlugin->getAllowedCountries(),
          'weight' => $configurationPlugin->getWeight(),
          'message_text' => $configurationPlugin->getMessageText(),
          'message_text_format' => $configurationPlugin->getMessageTextFormat(),
        ] + $basePluginDefinition;
      }
    }

    return $this->derivatives;
  }

}
