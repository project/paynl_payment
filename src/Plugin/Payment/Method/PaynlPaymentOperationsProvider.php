<?php

namespace Drupal\paynl_payment\Plugin\Payment\Method;

use Drupal\payment\Plugin\Payment\Method\PaymentMethodConfigurationOperationsProvider;

/**
 * Provides paynl_payment operations based on config entities.
 */
class PaynlPaymentOperationsProvider extends PaymentMethodConfigurationOperationsProvider {

  /**
   * {@inheritdoc}
   */
  protected function getPaymentMethodConfiguration($plugin_id) {
    $entity_id = substr($plugin_id, 14);

    return $this->paymentMethodConfigurationStorage->load($entity_id);
  }

}
