<?php

namespace Drupal\paynl_payment\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PaynlConfigForm.
 *
 * @package Drupal\paynl_payment\Form
 */
class PaynlConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'paynl_payment_paynl_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['paynl_payment.paynl_config'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('paynl_payment.paynl_config');

    $form['service_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service ID'),
      '#description' => $this->t('You can find your service ID in your Pay.nl dashboard.'),
      '#default_value' => $config->get('service_id') ? $config->get('service_id') : '',
    ];
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API token'),
      '#description' => $this->t('You can find your API token in your Pay.nl dashboard.'),
      '#default_value' => $config->get('api_key') ? $config->get('api_key') : '',
    ];
    $form['token_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API token code'),
      '#description' => $this->t('The API token code that corresponds with the API token above. You can find this in your Pay.nl dashboard.'),
      '#default_value' => $config->get('token_code') ? $config->get('token_code') : '',
    ];

    $form['test_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable test mode'),
      '#description' => $this->t('If enabled no real payments will be authorized. Be careful not to enable test mode in production environments.'),
      '#default_value' => $config->get('test_mode') ? $config->get('test_mode') : FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable('paynl_payment.paynl_config');

    if ($form_state->hasValue('service_id')) {
      $config->set('service_id', $form_state->getValue('service_id'));
    }
    if ($form_state->hasValue('api_key')) {
      $config->set('api_key', $form_state->getValue('api_key'));
    }
    if ($form_state->hasValue('token_code')) {
      $config->set('token_code', $form_state->getValue('token_code'));
    }

    if ($form_state->hasValue('test_mode')) {
      $config->set('test_mode', $form_state->getValue('test_mode'));
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
