<?php

namespace Drupal\paynl_payment\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\payment\Entity\PaymentInterface;
use Drupal\payment\Plugin\Payment\Status\PaymentStatusInterface;
use Drupal\paynl_payment\Helpers\PaynlSdkHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Webhook.
 *
 * @package Drupal\paynl_payment\Controller
 */
class Webhook extends ControllerBase {

  /**
   * Pay.nl SDK helper.
   *
   * @var \Drupal\paynl_payment\Helpers\PaynlSdkHelper
   */
  protected $paynlSdkHelper;

  /**
   * TestForm constructor.
   *
   * @param \Drupal\paynl_payment\Helpers\PaynlSdkHelper $paynlSdkHelper
   *   Pay.nl SDK helper.
   */
  public function __construct(PaynlSdkHelper $paynlSdkHelper) {
    $this->paynlSdkHelper = $paynlSdkHelper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('paynl_payment.helper')
    );
  }

  /**
   * Pay.nl is redirecting the visitor here after the payment process.
   *
   * We can use the Transaction wrapper to retrieve the transaction status.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   The payment object.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response for resuming the process.
   */
  public function returnHook(PaymentInterface $payment) {
    // Update payment status.
    $paymentStatus = $this->getPaymentStatus($payment, PaynlSdkHelper::PAYNL_TRANSACTION_PHASE_RETURN);
    $this->updatePaymentStatus($payment, $paymentStatus);

    // Return the response that resumes the payment process.
    return $payment->getPaymentType()->getResumeContextResponse()->getResponse();
  }

  /**
   * Pay.nl calls this after the payment status has been changed.
   *
   * We can use the Transaction wrapper to retrieve the transaction status.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   The payment object.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response object.
   */
  public function exchangeHook(PaymentInterface $payment) {
    // Update payment status.
    $paymentStatus = $this->getPaymentStatus($payment, PaynlSdkHelper::PAYNL_TRANSACTION_PHASE_EXCHANGE);
    $this->updatePaymentStatus($payment, $paymentStatus);

    // Return an empty response to the client.
    return new Response();
  }

  /**
   * Gets the payment status for a payment.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   The payment object.
   * @param string $phase
   *   The phase for the transaction.
   *
   * @return \Drupal\payment\Plugin\Payment\Status\PaymentStatusInterface
   *   A payment status object.
   */
  protected function getPaymentStatus(PaymentInterface $payment, $phase) {
    // Payment status mapping.
    $paymentStatus = ($this->paynlSdkHelper->isPaid($phase))
      ? 'payment_success' : (($this->paynlSdkHelper->isCanceled($phase))
        ? 'payment_cancelled' : 'payment_pending');

    return $payment->getPaymentMethod()->paymentStatusManager
      ->createInstance($paymentStatus);
  }

  /**
   * Updates the status of a payment.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   The payment object.
   * @param \Drupal\payment\Plugin\Payment\Status\PaymentStatusInterface $paymentStatus
   *   The payment status object.
   */
  protected function updatePaymentStatus(PaymentInterface $payment, PaymentStatusInterface $paymentStatus) {
    try {
      $payment
        ->setPaymentStatus($paymentStatus)
        ->save();
    }
    catch (\Exception $exception) {
      watchdog_exception('paynl_payment', $exception);
    }
  }

}
