# Pay.nl Payment
The Pay.nl Payment module for Drupal is an extension for the [Payment platform](https://www.drupal.org/project/payment).

## Introduction
[Pay.nl](https://www.pay.nl) is a Dutch payment service provider. This module integrates their
services with the [Payment platform](https://www.drupal.org/project/payment).

## Requirements
This module uses the [Pay.nl PHP SDK](https://github.com/paynl/sdk). When installing through
[Composer](https://getcomposer.org/) the SDK will be installed automatically. See next section.

## Installation
Install the module as you would normally install a contributed Drupal module. See
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules for directions on installing. Be aware that
this module has a Composer dependency that needs to be installed too.
See https://www.drupal.org/docs/8/extending-drupal-8/installing-modules-composer-dependencies for information on
installing Composer dependencies.

## Configuration

### API connection
For the connection with the Pay.nl API a service ID, an API token and an API token code need to be configured. Your can
find these in your Pay.nl dashboard. To configure them in Drupal navigate to Configuration >> Web Services >> Payment >> 
Pay.nl API configuration. Or go to /admin/config/services/payment/paynl-api directly.

You can run the connection in either test mode or live mode. Please note that in test mode no actual money will be
transferred so be sure to never enable test mode in a production environment.

In the status report (/admin/reports/status) you can see whether the connection to the API is working and whether the
connection is in live mode or test mode.

### Payment
On installation this module will automatically create a payment method in the Payment platform. To configure payment
methods in the Payment platform navigate to Configuration >> Web Services >> Payment >> Payment methods or go to
http://drupal8.paynl.test/admin/config/services/payment/method directly.

Multiple payment methods can be defined for Pay.nl. To add additional methods click the Add payment method configuration
button on the page mentioned in the previous paragraph. For each payment method defined within the Payment platform you
can configure the corresponding method to be used in the Pay.nl platform. The methods shown in the configuration form
are the methods that are enabled for the configured service ID. You can change these in your Pay.nl dashboard.

Per method you can configure a payment fee in an absolute amount or in a percentage of the total order amount. The fee
will be added to the amount to be paid.

Also note that to actually process payments in Drupal you also need a module that initializes payments. See
[Payment platform](https://www.drupal.org/project/payment) for more information.

## Contributing
Report issues with this module on https://www.drupal.org/project/issues/paynl_payment. There you can also supply patches
to fix issues.

### Maintainers
- Rico van de Vin ([ricovandevin](https://www.drupal.org/u/ricovandevin))
